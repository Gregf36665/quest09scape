# 2009Scape quest status

This repo keeps track of the status of all the implemented quests on [2009Scape](https://2009scape.org).
If you want to help test these quests head over to the [forums](https://forum.2009scape.org/viewtopic.php?t=36-test-server-status) and check the test server status.
You will also need to look at the [MRs](https://gitlab.com/2009scape/2009scape/merge_requests) to match the numbers up.
Use Ctrl-F to find a specific quest or look at the MegaView.

## Key

| Colour  | Meaning          |
|---------|------------------|
| Green   | Implemented      |
| Yellow  | Work in progress |
| Orange  | Dialogue found   | 
| White   | Not started      |
| Grey    | Revision 578     |



## MegaView

![All quests](images/00quests.png)

## Individual quest chains

### Enter the Abyss (miniquest)

![Enter the Abyss (miniquest)](images/EntertheAbyss(miniquest).png)

### Bar Crawl (miniquest)

![Bar Crawl (miniquest)](images/BarCrawl(miniquest).png)

### Cook's Assistant

![Cook's Assistant](images/Cook'sAssistant.png)

### Demon Slayer

![Demon Slayer](images/DemonSlayer.png)

### The Restless Ghost

![The Restless Ghost](images/TheRestlessGhost.png)

### Romeo & Juliet

![Romeo & Juliet](images/Romeo&Juliet.png)

### Sheep Shearer

![Sheep Shearer](images/SheepShearer.png)

### Shield of Arrav

![Shield of Arrav](images/ShieldofArrav.png)

### Ernest the Chicken

![Ernest the Chicken](images/ErnesttheChicken.png)

### Vampire Slayer

![Vampire Slayer](images/VampireSlayer.png)

### Imp Catcher

![Imp Catcher](images/ImpCatcher.png)

### Prince Ali Rescue

![Prince Ali Rescue](images/PrinceAliRescue.png)

### Doric's Quest

![Doric's Quest](images/Doric'sQuest.png)

### Black Knights' Fortress

![Black Knights' Fortress](images/BlackKnights'Fortress.png)

### Witch's Potion

![Witch's Potion](images/Witch'sPotion.png)

### The Knight's Sword

![The Knight's Sword](images/TheKnight'sSword.png)

### Goblin Diplomacy

![Goblin Diplomacy](images/GoblinDiplomacy.png)

### Pirate's Treasure

![Pirate's Treasure](images/Pirate'sTreasure.png)

### Dragon Slayer

![Dragon Slayer](images/DragonSlayer.png)

### Druidic Ritual

![Druidic Ritual](images/DruidicRitual.png)

### Lost City

![Lost City](images/LostCity.png)

### Witch's House

![Witch's House](images/Witch'sHouse.png)

### Merlin's Crystal

![Merlin's Crystal](images/Merlin'sCrystal.png)

### Heroes' Quest

![Heroes' Quest](images/Heroes'Quest.png)

### Scorpion Catcher

![Scorpion Catcher](images/ScorpionCatcher.png)

### Family Crest

![Family Crest](images/FamilyCrest.png)

### Tribal Totem

![Tribal Totem](images/TribalTotem.png)

### Fishing Contest

![Fishing Contest](images/FishingContest.png)

### Monk's Friend

![Monk's Friend](images/Monk'sFriend.png)

### Temple of Ikov

![Temple of Ikov](images/TempleofIkov.png)

### Clock Tower

![Clock Tower](images/ClockTower.png)

### Holy Grail

![Holy Grail](images/HolyGrail.png)

### Tree Gnome Village

![Tree Gnome Village](images/TreeGnomeVillage.png)

### Fight Arena

![Fight Arena](images/FightArena.png)

### Hazeel Cult

![Hazeel Cult](images/HazeelCult.png)

### Sheep Herder

![Sheep Herder](images/SheepHerder.png)

### Plague City

![Plague City](images/PlagueCity.png)

### Sea Slug

![Sea Slug](images/SeaSlug.png)

### Waterfall Quest

![Waterfall Quest](images/WaterfallQuest.png)

### Biohazard

![Biohazard](images/Biohazard.png)

### Jungle Potion

![Jungle Potion](images/JunglePotion.png)

### The Grand Tree

![The Grand Tree](images/TheGrandTree.png)

### Shilo Village

![Shilo Village](images/ShiloVillage.png)

### Underground Pass

![Underground Pass](images/UndergroundPass.png)

### Observatory Quest

![Observatory Quest](images/ObservatoryQuest.png)

### The Tourist Trap

![The Tourist Trap](images/TheTouristTrap.png)

### Watchtower

![Watchtower](images/Watchtower.png)

### Dwarf Cannon

![Dwarf Cannon](images/DwarfCannon.png)

### Murder Mystery

![Murder Mystery](images/MurderMystery.png)

### The Dig Site

![The Dig Site](images/TheDigSite.png)

### Gertrude's Cat

![Gertrude's Cat](images/Gertrude'sCat.png)

### Legends' Quest

![Legends' Quest](images/Legends'Quest.png)

### Rune Mysteries

![Rune Mysteries](images/RuneMysteries.png)

### Big Chompy Bird Hunting

![Big Chompy Bird Hunting](images/BigChompyBirdHunting.png)

### Elemental Workshop I

![Elemental Workshop I](images/ElementalWorkshopI.png)

### Priest in Peril

![Priest in Peril](images/PriestinPeril.png)

### Nature Spirit

![Nature Spirit](images/NatureSpirit.png)

### Death Plateau

![Death Plateau](images/DeathPlateau.png)

### Troll Stronghold

![Troll Stronghold](images/TrollStronghold.png)

### Tai Bwo Wannai Trio

![Tai Bwo Wannai Trio](images/TaiBwoWannaiTrio.png)

### Regicide

![Regicide](images/Regicide.png)

### Eadgar's Ruse

![Eadgar's Ruse](images/Eadgar'sRuse.png)

### Shades of Mort'ton

![Shades of Mort'ton](images/ShadesofMort'ton.png)

### The Fremennik Trials

![The Fremennik Trials](images/TheFremennikTrials.png)

### Horror from the Deep

![Horror from the Deep](images/HorrorfromtheDeep.png)

### Throne of Miscellania

![Throne of Miscellania](images/ThroneofMiscellania.png)

### Monkey Madness

![Monkey Madness](images/MonkeyMadness.png)

### Haunted Mine

![Haunted Mine](images/HauntedMine.png)

### Troll Romance

![Troll Romance](images/TrollRomance.png)

### In Search of the Myreque

![In Search of the Myreque](images/InSearchoftheMyreque.png)

### Creature of Fenkenstrain

![Creature of Fenkenstrain](images/CreatureofFenkenstrain.png)

### Roving Elves

![Roving Elves](images/RovingElves.png)

### Ghosts Ahoy

![Ghosts Ahoy](images/GhostsAhoy.png)

### One Small Favour

![One Small Favour](images/OneSmallFavour.png)

### Mountain Daughter

![Mountain Daughter](images/MountainDaughter.png)

### Between a Rock...

![Between a Rock...](images/BetweenaRock....png)

### The Feud

![The Feud](images/TheFeud.png)

### The Golem

![The Golem](images/TheGolem.png)

### Desert Treasure

![Desert Treasure](images/DesertTreasure.png)

### Icthlarin's Little Helper

![Icthlarin's Little Helper](images/Icthlarin'sLittleHelper.png)

### Tears of Guthix

![Tears of Guthix](images/TearsofGuthix.png)

### Zogre Flesh Eaters

![Zogre Flesh Eaters](images/ZogreFleshEaters.png)

### The Lost Tribe

![The Lost Tribe](images/TheLostTribe.png)

### The Giant Dwarf

![The Giant Dwarf](images/TheGiantDwarf.png)

### Recruitment Drive

![Recruitment Drive](images/RecruitmentDrive.png)

### Mourning's End Part I

![Mourning's End Part I](images/Mourning'sEndPartI.png)

### Forgettable Tale of a Drunken Dwarf

![Forgettable Tale of a Drunken Dwarf](images/ForgettableTaleofaDrunkenDwarf.png)

### Garden of Tranquillity

![Garden of Tranquillity](images/GardenofTranquillity.png)

### A Tail of Two Cats

![A Tail of Two Cats](images/ATailofTwoCats.png)

### Wanted!

![Wanted!](images/Wanted!.png)

### Mourning's End Part II

![Mourning's End Part II](images/Mourning'sEndPartII.png)

### Rum Deal

![Rum Deal](images/RumDeal.png)

### Shadow of the Storm

![Shadow of the Storm](images/ShadowoftheStorm.png)

### Making History

![Making History](images/MakingHistory.png)

### Rat Catchers

![Rat Catchers](images/RatCatchers.png)

### Spirits of the Elid

![Spirits of the Elid](images/SpiritsoftheElid.png)

### Devious Minds

![Devious Minds](images/DeviousMinds.png)

### The Hand in the Sand

![The Hand in the Sand](images/TheHandintheSand.png)

### Enakhra's Lament

![Enakhra's Lament](images/Enakhra'sLament.png)

### Cabin Fever

![Cabin Fever](images/CabinFever.png)

### A Fairy Tale I - Growing Pains

![A Fairy Tale I - Growing Pains](images/AFairyTaleI-GrowingPains.png)

### Recipe for Disaster

![Recipe for Disaster](images/RecipeforDisaster.png)

### In Aid of the Myreque

![In Aid of the Myreque](images/InAidoftheMyreque.png)

### A Soul's Bane

![A Soul's Bane](images/ASoul'sBane.png)

### Rag and Bone Man

![Rag and Bone Man](images/RagandBoneMan.png)

### Swan Song

![Swan Song](images/SwanSong.png)

### Royal Trouble

![Royal Trouble](images/RoyalTrouble.png)

### Death to the Dorgeshuun

![Death to the Dorgeshuun](images/DeathtotheDorgeshuun.png)

### A Fairy Tale II - Cure a Queen

![A Fairy Tale II - Cure a Queen](images/AFairyTaleII-CureaQueen.png)

### Lunar Diplomacy

![Lunar Diplomacy](images/LunarDiplomacy.png)

### The Eyes of Glouphrie

![The Eyes of Glouphrie](images/TheEyesofGlouphrie.png)

### The Darkness of Hallowvale

![The Darkness of Hallowvale](images/TheDarknessofHallowvale.png)

### The Slug Menace

![The Slug Menace](images/TheSlugMenace.png)

### Elemental Workshop II

![Elemental Workshop II](images/ElementalWorkshopII.png)

### My Arm's Big Adventure

![My Arm's Big Adventure](images/MyArm'sBigAdventure.png)

### Enlightened Journey

![Enlightened Journey](images/EnlightenedJourney.png)

### Eagles' Peak

![Eagles' Peak](images/Eagles'Peak.png)

### Animal Magnetism

![Animal Magnetism](images/AnimalMagnetism.png)

### Contact!

![Contact!](images/Contact!.png)

### Cold War

![Cold War](images/ColdWar.png)

### The Fremennik Isles

![The Fremennik Isles](images/TheFremennikIsles.png)

### Tower of Life

![Tower of Life](images/TowerofLife.png)

### The Great Brain Robbery

![The Great Brain Robbery](images/TheGreatBrainRobbery.png)

### What Lies Below

![What Lies Below](images/WhatLiesBelow.png)

### Olaf's Quest

![Olaf's Quest](images/Olaf'sQuest.png)

### Another Slice of H.A.M.

![Another Slice of H.A.M.](images/AnotherSliceofH.A.M..png)

### Dream Mentor

![Dream Mentor](images/DreamMentor.png)

### Grim Tales

![Grim Tales](images/GrimTales.png)

### King's Ransom

![King's Ransom](images/King'sRansom.png)

### The Path of Glouphrie

![The Path of Glouphrie](images/ThePathofGlouphrie.png)

### Back to my Roots

![Back to my Roots](images/BacktomyRoots.png)

### Land of the Goblins

![Land of the Goblins](images/LandoftheGoblins.png)

### Dealing with Scabaras

![Dealing with Scabaras](images/DealingwithScabaras.png)

### Wolf Whistle

![Wolf Whistle](images/WolfWhistle.png)

### As a First Resort

![As a First Resort](images/AsaFirstResort.png)

### Catapult Construction

![Catapult Construction](images/CatapultConstruction.png)

### Kennith's Concerns

![Kennith's Concerns](images/Kennith'sConcerns.png)

### Legacy of Seergaze

![Legacy of Seergaze](images/LegacyofSeergaze.png)

### Perils of Ice Mountain

![Perils of Ice Mountain](images/PerilsofIceMountain.png)

### TokTz-Ket-Dill

![TokTz-Ket-Dill](images/TokTz-Ket-Dill.png)

### Smoking Kills

![Smoking Kills](images/SmokingKills.png)

### Rocking Out

![Rocking Out](images/RockingOut.png)

### Spirit of Summer

![Spirit of Summer](images/SpiritofSummer.png)

### Meeting History

![Meeting History](images/MeetingHistory.png)

### All Fired Up

![All Fired Up](images/AllFiredUp.png)

### Summer's End

![Summer's End](images/Summer'sEnd.png)

### Defender of Varrock

![Defender of Varrock](images/DefenderofVarrock.png)

### Swept Away

![Swept Away](images/SweptAway.png)

### While Guthix Sleeps

![While Guthix Sleeps](images/WhileGuthixSleeps.png)

### Myths of the White Lands

![Myths of the White Lands](images/MythsoftheWhiteLands.png)

### In Pyre Need

![In Pyre Need](images/InPyreNeed.png)

### The Chosen Commander

![The Chosen Commander](images/TheChosenCommander.png)

### Glorious Memories

![Glorious Memories](images/GloriousMemories.png)

### The Tale of the Muspah

![The Tale of the Muspah](images/TheTaleoftheMuspah.png)

### Missing My Mummy

![Missing My Mummy](images/MissingMyMummy.png)

### Hunt for Red Raktuber

![Hunt for Red Raktuber](images/HuntforRedRaktuber.png)

### The Curse of Arrav

![The Curse of Arrav](images/TheCurseofArrav.png)

### Fur 'n Seek

![Fur 'n Seek](images/Fur'nSeek.png)

### Forgiveness of a Chaos Dwarf

![Forgiveness of a Chaos Dwarf](images/ForgivenessofaChaosDwarf.png)

### Within the Light

![Within the Light](images/WithintheLight.png)

### The Temple at Senntisten

![The Temple at Senntisten](images/TheTempleatSenntisten.png)

### Blood Runs Deep

![Blood Runs Deep](images/BloodRunsDeep.png)

### Recipe for Disaster: Freeing Pirate Pete

![Recipe for Disaster: Freeing Pirate Pete](images/RecipeforDisasterFreeingPiratePete.png)

### Recipe for Disaster: Another Cook's Quest

![Recipe for Disaster: Another Cook's Quest](images/RecipeforDisasterAnotherCook'sQuest.png)

### The Hunt for Surok (miniquest)

![The Hunt for Surok (miniquest)](images/TheHuntforSurok(miniquest).png)


