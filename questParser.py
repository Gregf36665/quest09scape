#!/usr/bin/env python3
import sqlite3
import time

headers = {'user-agent': 'quest-dependency-graph'}


def create_tables(con: sqlite3.Connection, new: bool):
    if new:
        con.execute("DROP TABLE IF EXISTS questStatus")

    con.execute("CREATE TABLE questStatus ("
                "QuestID INTEGER UNIQUE,"
                "Name TEXT NOT NULL,"
                "Dialogue INTEGER,"
                "Started INTEGER,"
                "Implemented INTEGER,"
                "Comments TEXT,"
                "CacheNumber"
                "INTEGER, "
                "PRIMARY KEY(QuestID)"
                ")")


quest_map_578 = {
    160: "Blood Runs Deep",
    159: "The Temple at Senntisten",
    158: "Within the Light",
    157: "Forgiveness of a Chaos Dwarf",
    156: "Fur 'n Seek",
    155: "The Curse of Arrav",
    154: "Hunt for Red Raktuber",
    153: "Missing My Mummy",
    152: "The Tale of the Muspah",
    151: "Glorious Memories",
    150: "The Chosen Commander",
}

questmap_530 = {
    149: "In Pyre Need",
    148: "Myths of the White Lands",
    147: "While Guthix Sleeps",
    146: "Swept Away",
    145: "Defender of Varrock",
    144: "Summer's End",
    143: "All Fired Up",
    142: "Meeting History",
    141: "Spirit of Summer",
    140: "Rocking Out",
    139: "Smoking Kills",
    138: "TokTz-Ket-Dill",
    137: "Perils of Ice Mountain",
    136: "Legacy of Seergaze",
    135: "Kennith's Concerns",
    134: "Catapult Construction",
    133: "As a First Resort",
    132: "Wolf Whistle",
    131: "Dealing with Scabaras",
    130: "Land of the Goblins",
    129: "Back to my Roots",
    128: "The Path of Glouphrie",
    127: "King's Ransom",
    126: "Grim Tales",
    125: "Dream Mentor",
    124: "Another Slice of H.A.M.",
    123: "Olaf's Quest",
    122: "What Lies Below",
    121: "The Great Brain Robbery",
    120: "Tower of Life",
    119: "The Fremennik Isles",
    118: "Cold War",
    117: "Contact!",
    116: "Animal Magnetism",
    115: "Eagles' Peak",
    114: "Enlightened Journey",
    113: "My Arm's Big Adventure",
    112: "Elemental Workshop II",
    111: "The Slug Menace",
    110: "The Darkness of Hallowvale",
    109: "The Eyes of Glouphrie",
    108: "Lunar Diplomacy",
    107: "A Fairy Tale II - Cure a Queen",
    106: "Death to the Dorgeshuun",
    105: "Royal Trouble",
    104: "Swan Song",
    103: "Rag and Bone Man",
    102: "A Soul's Bane",
    101: "In Aid of the Myreque",
    100: "Recipe for Disaster",
    99: "A Fairy Tale I - Growing Pains",
    98: "Cabin Fever",
    97: "Enakhra's Lament",
    96: "The Hand in the Sand",
    95: "Devious Minds",
    94: "Spirits of the Elid",
    93: "Rat Catchers",
    92: "Making History",
    91: "Shadow of the Storm",
    90: "Rum Deal",
    89: "Mourning's End Part II",
    88: "Wanted!",
    87: "A Tail of Two Cats",
    86: "Garden of Tranquillity",
    85: "Forgettable Tale of a Drunken Dwarf",
    84: "Mourning's End Part I",
    83: "Recruitment Drive",
    82: "The Giant Dwarf",
    81: "The Lost Tribe",
    80: "Zogre Flesh Eaters",
    79: "Tears of Guthix",
    78: "Icthlarin's Little Helper",
    77: "Desert Treasure",
    76: "The Golem",
    75: "The Feud",
    74: "Between a Rock...",
    73: "Mountain Daughter",
    72: "One Small Favour",
    71: "Ghosts Ahoy",
    70: "Roving Elves",
    69: "Creature of Fenkenstrain",
    68: "In Search of the Myreque",
    67: "Troll Romance",
    66: "Haunted Mine",
    65: "Monkey Madness",
    64: "Throne of Miscellania",
    63: "Horror from the Deep",
    62: "The Fremennik Trials",
    61: "Shades of Mort'ton",
    60: "Eadgar's Ruse",
    59: "Regicide",
    58: "Tai Bwo Wannai Trio",
    57: "Troll Stronghold",
    56: "Death Plateau",
    55: "Nature Spirit",
    54: "Priest in Peril",
    53: "Elemental Workshop I",
    52: "Big Chompy Bird Hunting",
    51: "Rune Mysteries",
    50: "Legends' Quest",
    49: "Gertrude's Cat",
    48: "The Dig Site",
    47: "Murder Mystery",
    46: "Dwarf Cannon",
    45: "Watchtower",
    44: "The Tourist Trap",
    43: "Observatory Quest",
    42: "Underground Pass",
    41: "Shilo Village",
    40: "The Grand Tree",
    39: "Jungle Potion",
    38: "Biohazard",
    37: "Waterfall Quest",
    36: "Sea Slug",
    35: "Plague City",
    34: "Sheep Herder",
    33: "Hazeel Cult",
    32: "Fight Arena",
    31: "Tree Gnome Village",
    30: "Holy Grail",
    29: "Clock Tower",
    28: "Temple of Ikov",
    27: "Monk's Friend",
    26: "Fishing Contest",
    25: "Tribal Totem",
    24: "Family Crest",
    23: "Scorpion Catcher",
    22: "Heroes' Quest",
    21: "Merlin's Crystal",
    20: "Witch's House",
    19: "Lost City",
    18: "Druidic Ritual",
    17: "Dragon Slayer",
    16: "Pirate's Treasure",
    15: "Goblin Diplomacy",
    14: "The Knight's Sword",
    13: "Witch's Potion",
    12: "Black Knights' Fortress",
    11: "Doric's Quest",
    10: "Prince Ali Rescue",
    9: "Imp Catcher",
    8: "Vampire Slayer",
    7: "Ernest the Chicken",
    6: "Shield of Arrav",
    5: "Sheep Shearer",
    4: "Romeo & Juliet",
    3: "The Restless Ghost",
    2: "Demon Slayer",
    1: "Cook's Assistant",
    -2: 'Enter the Abyss (miniquest)',
    -1: 'Bar Crawl (miniquest)',

    1001: "Recipe for Disaster: Freeing Pirate Pete",
    1002: "Recipe for Disaster: Another Cook's Quest",
    1003: "The Hunt for Surok (miniquest)",

}

questmap = dict(questmap_530)
questmap.update(quest_map_578)

implemented = {
    "All Fired Up",
    "Animal Magnetism",
    "Bar Crawl (miniquest)",
    "Big Chompy Bird Hunting",
    "Black Knights' Fortress",
    "Clock Tower",
    "Cook's Assistant",
    "Creature of Fenkenstrain",
    "Death Plateau",
    "Demon Slayer",
    "Doric's Quest",
    "Dragon Slayer",
    "Druidic Ritual",
    "Dwarf Cannon",
    "Elemental Workshop I",
    "Ernest the Chicken",
    "Family Crest",
    "Fight Arena",
    "Fishing Contest",
    "Gertrude's Cat",
    "Goblin Diplomacy",
    "Imp Catcher",
    "Jungle Potion",
    "Lost City",
    "Merlin's Crystal",
    "Monk's Friend",
    "Nature Spirit",
    "Pirate's Treasure",
    "Plague City",
    "Priest in Peril",
    "Prince Ali Rescue",
    "Rag and Bone Man",
    "Recruitment Drive",
    "Romeo & Juliet",
    "Roving Elves",
    "Rune Mysteries",
    "Scorpion Catcher",
    "Sheep Herder",
    "Sheep Shearer",
    "Shield of Arrav",
    "Temple of Ikov",
    "The Dig Site",
    "The Fremennik Trials",
    "The Golem",
    "The Grand Tree",
    "The Knight's Sword",
    "The Lost Tribe",
    "The Restless Ghost",
    "The Tourist Trap",
    "Tree Gnome Village",
    "Tribal Totem",
    "Troll Stronghold",
    "Vampire Slayer",
    "Waterfall Quest",
    "What Lies Below",
    "Witch's House",
    "Witch's Potion",
    "Wolf Whistle",
    'Enter the Abyss (miniquest)',
}

has_dialogue = {
    "Creature of Fenkenstrain",
    "Enlightened Journey",
    "Garden of Tranquillity",
    "Horror from the Deep",
    "Murder Mystery",
    "Shadow of the Storm",
    "Tears of Guthix",
    "Temple of Ikov",
    "The Dig Site",
    "The Giant Dwarf",
    "Tower of Life",
    "Mountain Daughter",
    "Death to the Dorgeshuun",
    "In Pyre Need",
    "Legacy of Seergaze",
    "Spirits of the Elid",
    "Underground Pass",
    "Regicide",
    "Rum Deal",
    "Mourning's End Part II",
    "Mourning's End Part I",
    "Dream Mentor",
    "A Fairy Tale II - Cure a Queen",
    "A Fairy Tale I - Growing Pains",
    "Summer's End",
    "Throne of Miscellania",
    "The Fremennik Isles",
    "Olaf's Quest",
    "Contact!",
    "Shadow of the Storm",
}

in_progress = {
    "Biohazard",
    "Desert Treasure",
    "Heroes' Quest",
    "Holy Grail",
    "Monkey Madness",
    "Observatory Quest",
    "Sea Slug",
    "Tears of Guthix",
    "Zogre Flesh Eaters",
}


def store_quests(q_map: dict, con: sqlite3.Connection, rev):
    for quest_id, quest_name in q_map.items():
        print(quest_id, quest_name)
        resp = [
            quest_id,
            quest_name,
            quest_name in has_dialogue,
            quest_name in in_progress,
            quest_name in implemented,
            "",
            rev
        ]
        con.execute("INSERT into questStatus VALUES (?, ?, ?, ?, ?, ?, ?)", resp)


def verify_set(list_of_quests: set):
    for v in list_of_quests:
        if v not in questmap.values():
            raise ValueError(f"{v} is not a valid quest name")
    pass


if __name__ == '__main__':
    print("Verifying the implemented list")
    verify_set(implemented)
    print("Verifying the in progress list")
    verify_set(in_progress)
    print("Verifying the dialogue list")
    verify_set(has_dialogue)
    with sqlite3.connect("questDep.db") as conn:
        create_tables(conn, True)
        store_quests(questmap_530, conn, 530)
        store_quests(quest_map_578, conn, 578)

