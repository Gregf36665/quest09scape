rm images/* 2>/dev/null
mkdir -p dots
mkdir -p images
pushd dots
dot_files=`ls *.dot`
popd
for f in $dot_files; do
  echo "Converting: $f"
	/c/Program\ Files/Graphviz/bin/dot.exe -Tpng dots/$f -o images/"${f%.*}".png
done

