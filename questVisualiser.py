import sqlite3
from questParser import questmap


def format_depts(quest_id: int, conn, recursive=False) -> [str]:
    output = colour_map(quest_id, conn)
    resp = get_deps(quest_id, conn)
    for req in resp:
        output += f"\"{questmap[req]}\" -> \"{questmap[quest_id]}\"; \n"
        if recursive:
            output += colour_map(quest_id, conn)
            output += format_depts(req, conn, recursive)
    return output


def get_deps(quest_id: int, conn) -> [str, None]:
    resp = conn.execute("SELECT RequirementID from quests where QuestID = (?)", (quest_id,)).fetchall()
    if len(resp) == 0:
        return []
    else:
        deps = []
        for r in resp:
            req = questmap[int(r[0])]
            deps += [int(r[0])]
            deps += get_deps(req, conn)
        return deps


def create_graph(quest_id: int, conn, recursive=False) -> [str, None]:
    output = ""
    resp = format_depts(quest_id, conn, recursive)
    if resp.count('\n') == 1:
        return resp
    else:
        output += resp
        return output


def colour_map(quest_id: int, conn) -> str:
    _, name, dialogue, started, implemented, _, cache_number = \
        conn.execute("SELECT * from questStatus where QuestID = (?)", (quest_id,)).fetchall()[0]

    if implemented:
        colour = "green"
    elif started:
        colour = "yellow"
    elif dialogue:
        colour = "orange"
    elif cache_number > 530:
        colour = "grey"
    else:
        colour = "white"

    return f'"{name}" [color=black style=filled fillcolor={colour}];\n'


def draw_all_on_one(conn):
    all_quests = ""
    resp = conn.execute("SELECT Name, QuestID from questStatus").fetchall()
    for name, quest_id in resp:
        data = create_graph(quest_id, conn)
        all_quests += data

    with open(f"dots/00quests.dot", 'w') as f:
        f.write('digraph{ \nrankdir = "LR"; \n')
        f.write(all_quests)
        f.write("}")


def draw_individual(conn):
    resp = conn.execute("SELECT Name, QuestID from questStatus").fetchall()
    for name, quest_id in resp:
        no_space = name.replace(" ", "")
        no_space = no_space.replace(":", "")
        data = create_graph(quest_id, conn, recursive=True)
        output = "digraph { \n"
        output += data
        output += "}"
        with open(f"dots/{no_space}.dot", 'w') as f:
            f.write(output)


def main():
    with sqlite3.connect("questDep.db") as conn:
        draw_all_on_one(conn)
        draw_individual(conn)


if __name__ == '__main__':
    main()
